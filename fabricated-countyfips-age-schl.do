* extract age and educational attainment variables from 2016, 5-year PUMS of ACS
use <<2016, 5-year ACS file from IPUMS with person-level age and schl variables>>, clear
keep us2016c_*
gen r = runiform()
sort r
drop r
tempfile features
save `features', replace
count
local acsn = `r(N)'

* bring in sheet of US counties and their total population counts
import excel <<county-level file with countyfips and population of eachcounty>>, sheet("Sheet") firstrow clear
* simulate person-level records for about 1/20th of US population
replace pop = ceil(pop / 20)
expand pop
keep countyfips

gen r = runiform()
sort r
drop r
* keep the number of rows to match PUMS extract above
keep in 1/`acsn'
* merge age and educational attainment, now in random order, into the county file
merge 1:1 _n using `features', keepusing(us2016c_*) nogen
destring us2016c_agep, gen(age)
replace countyfips = string(countyfips, "%05.0f")
ren us2016c_schl schl
keep countyfips age schl
order co* age schl
export delimited using "fabricated-countyfips-age-schl.csv", replace
